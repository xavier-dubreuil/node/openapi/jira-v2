import { expect, describe, it } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import { Container } from '../src';
import { paramsSerializer } from '../src/container';

const content = 'content';
const axiosRequest = {
  method: 'get',
  url: '/rest/api/2/serverInfo',
  data: undefined,
  headers: {},
  params: {}
};

describe('OpenAPI', () => {
  it('getServerInfo', async () => {
    const container = await Container.new('http://localhost', 'username', 'password');
    const promise = container.client.getServerInfo();
    expect(mockAxios.request).toHaveBeenCalledWith(axiosRequest);
    const response = { data: content };
    mockAxios.mockResponse(response);
    const { data } = await promise;
    expect(data).toEqual(content);
  });
  it('ParamSerializer', async () => {
    const check = paramsSerializer({ key: ['val1', 'val2'] });
    expect(check).toEqual('key=val1%2Cval2');
  });
});
