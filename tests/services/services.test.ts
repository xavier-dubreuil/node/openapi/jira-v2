import { expect, it } from '@jest/globals';
import OpenAPIClientAxios from 'openapi-client-axios';
import { Client } from '../../src';
import { JiraService } from '../../src/services/service';
import { getDefinition } from '../../src/container';
interface TestType {
  key: string | number;
}

export class Service extends JiraService<TestType> {
  public async fetchRecord (key: string | number): Promise<TestType | null> {
    return new Promise(resolve => {
      if (key === 'random') {
        resolve({ key: Math.floor(Math.random()) });
        return;
      }
      if (key === 'null') {
        resolve(null);
        return;
      }
      resolve({ key });
    });
  }
}

export class EmptyService extends JiraService<TestType> {
}

describe('Service Users', () => {
  it('Get Record', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new Service(client);
    const item1 = await service.getRecord('random');
    const item2 = await service.getRecord('random');
    expect(item2).toEqual(item1);
    const nullable = await service.getRecord('null');
    expect(nullable).toBeNull();
  });
  it('Fetch Record', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new EmptyService(client);
    await expect(service.fetchRecord('test')).rejects.toEqual(Error('Unhandled fetch for key: test'));
  });
});
