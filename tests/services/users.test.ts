import { expect, describe, it } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import { UsersService } from '../../src/services';
import OpenAPIClientAxios from 'openapi-client-axios';
import { Client } from '../../src';
import { getDefinition } from '../../src/container';

const fetchKey = 'test';
const content = 'content';
const axiosRequest = { 
  method: 'get',
  url: '/rest/api/2/user',
  data: undefined,
  headers: {},
  params: {
    accountId: fetchKey
  }
};

describe('Service Statuses', () => {
  it('Fetch Record', async () => {
    const api = new OpenAPIClientAxios({ definition: await getDefinition() });
    const client = await api.init<Client>();
    const service = new UsersService(client);
    const promise = service.fetchRecord(fetchKey);
    expect(mockAxios.request).toHaveBeenCalledWith(axiosRequest);
    const response = { data: content };
    mockAxios.mockResponse(response);
    const data = await promise;
    expect(data).toEqual(content);
  });
});
