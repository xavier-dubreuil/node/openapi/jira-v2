import { JiraService } from "./service";
import { Components } from '../openapi.d';

export class StatusesService extends JiraService<Components.Schemas.Status> {
  public async fetchRecord (key: string | number) {
    const { data: status } = await this.client.getStatus({ idOrName: key as string });
    return status;
  }
}
