import { ClientService } from '@openapi/container';
import { Client } from '../openapi.d';

export abstract class JiraService<T> extends ClientService<Client> {

  protected records: Record<string | number, T> = {};

  constructor (protected readonly client: Client) {
    super(client);
  }

  public async fetchRecord (key: string | number): Promise<T | null> {
    return Promise.reject(Error(`Unhandled fetch for key: ${key}`));
  }

  public async getRecord (key: string | number): Promise<T | null> {
    if (this.records[key] == undefined) {
      const value = await this.fetchRecord(key);
      if (value === null) {
        return null;
      }
      this.records[key] = value;
    }
    return this.records[key];
  }
}
