import { JiraService } from "./service";
import { Components } from '../openapi.d';

export class UsersService extends JiraService<Components.Schemas.User> {
  public async fetchRecord (key: string | number) {
    const { data: user } = await this.client.getUser({ accountId: key as string });
    return user;
  }
}
